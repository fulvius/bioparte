program sirv

  implicit none

  integer :: k, stop_step
  real :: h, Tf
  real :: lambda, gamma, beta
  real :: s0 = 100000.0, i0 = 1.0, r0 = 0.0, v0=1.0
  real, dimension(:), allocatable :: s, i, r, v, t

  h = 0.001
  Tf = 25.0

  ! rates
  lambda = 0.00035
  gamma = 0.1
  beta = 0.4

  allocate(s(1:int(Tf/h)))
  allocate(i(1:int(Tf/h)))
  allocate(r(1:int(Tf/h)))
  allocate(v(1:int(tf/h)))
  allocate(t(1:int(Tf/h)))

  ! initial conditions
  s(1) = s0
  i(1) = i0
  r(1) = r0
  v(1) = v0
  t(1) = 0.0

  do k = 1, int(Tf/h)-1, 1

    call RK4(s(k), i(k), r(k), v(k), lambda, gamma, beta, h, s(k+1), i(k+1), r(k+1), v(k+1))
    t(k+1) = (k+1)*h

    if (s(k+1) <= 0) then
      stop_step = k
      exit
    end if

  end do

  ! save data
  open(1, file='data.dat')
  do k = 1, stop_step, 1
    write (1,*) t(k), s(k), i(k), r(k), v(k)
  end do
  close(1)

  ! save model type
  open(2, file='model_type.txt')
  write(2,*) 'sirv'
  close(2)


contains

  real function Sfun(s, i, r, v, lambda, beta)
    implicit none
    real :: s, i, r, v
    real :: lambda, beta
    Sfun = - lambda * s * i - beta * v
  end function Sfun

  real function Ifun(s, i, r, v, lambda, gamma)
    implicit none
    real :: s, i, r, v
    real :: lambda, gamma
    Ifun = lambda * s * i - gamma * i
  end function Ifun

  real function Rfun(s, i, r, v, gamma)
    implicit none
    real :: s, i, r, v
    real :: gamma
    Rfun = gamma * i
  end function Rfun

  real function Vfun(s, i, r, v, beta)
    implicit none
    real :: s, i, r, v
    real :: beta
    Vfun = beta * v
  end function Vfun

  subroutine RK4(s, i, r, v, lambda, gamma, beta, h, s_n, i_n, r_n, v_n)
    implicit none
    real :: s, i, r, v, lambda, gamma, beta, h, s_n, i_n, r_n, v_n
    real :: k1_s, k2_s, k3_s, k4_s, k1_i, k2_i, k3_i, k4_i, k1_r, k2_r, k3_r, k4_r, k1_v, k2_v, k3_v, k4_v

    k1_s = Sfun(s, i, r, v, lambda, beta)
    k1_i = Ifun(s, i, r, v, lambda, gamma)
    k1_r = Rfun(s, i, r, v, gamma)
    k1_v = Vfun(s, i, r, v, beta)

    k2_s = Sfun(s+k1_s*h/2.0, i+k1_i*h/2.0, r+k1_r*h/2.0, v+k1_v*h/2.0, lambda, beta)
    k2_i = Ifun(s+k1_s*h/2.0, i+k1_i*h/2.0, r+k1_r*h/2.0, v+k1_v*h/2.0, lambda, gamma)
    k2_r = Rfun(s+k1_s*h/2.0, i+k1_i*h/2.0, r+k1_r*h/2.0, v+k1_v*h/2.0, gamma)
    k2_v = Vfun(s+k1_s*h/2.0, i+k1_i*h/2.0, r+k1_r*h/2.0, v+k1_v*h/2.0, beta)

    k3_s = Sfun(s+k2_s*h/2.0, i+k2_i*h/2.0, r+k2_r*h/2.0, v+k2_v*h/2.0, lambda, beta)
    k3_i = Ifun(s+k2_s*h/2.0, i+k2_i*h/2.0, r+k2_r*h/2.0, v+k2_v*h/2.0, lambda, gamma)
    k3_r = Rfun(s+k2_s*h/2.0, i+k2_i*h/2.0, r+k2_r*h/2.0, v+k2_v*h/2.0, gamma)
    k3_v = Vfun(s+k2_s*h/2.0, i+k2_i*h/2.0, r+k2_r*h/2.0, v+k2_v*h/2.0, beta)

    k4_s = Sfun(s+k3_s*h, i+k3_i*h, r+k3_r*h, v+k3_v*h, lambda, beta)
    k4_i = Ifun(s+k3_s*h, i+k3_i*h, r+k3_r*h, v+k3_v*h, lambda, gamma)
    k4_r = Rfun(s+k3_s*h, i+k3_i*h, r+k3_r*h, v+k3_v*h, gamma)
    k4_v = Vfun(s+k3_s*h, i+k3_i*h, r+k3_r*h, v+k3_v*h, beta)

    s_n = s + h * (k1_s + 2*k2_s + 2*k3_s + k4_s)/6.0
    i_n = i + h * (k1_i + 2*k2_i + 2*k3_i + k4_i)/6.0
    r_n = r + h * (k1_r + 2*k2_r + 2*k3_r + k4_r)/6.0
    v_n = v + h * (k1_v + 2*k2_v + 2*k3_v + k4_v)/6.0

  end subroutine RK4

end program sirv
