program seir

  implicit none

  integer :: k
  real :: h, Tf
  real :: lambda, gamma, nu
  real :: s0 = 10000.0, e0 = 1.0,  i0 = 1.0, r0 = 0.0
  real, dimension(:), allocatable :: s, e, i, r, t

  h = 0.001
  Tf = 200.0

  ! rates
  lambda = 0.00035
  gamma = 0.01
  nu = 0.005

  allocate(s(1:int(Tf/h)))
  allocate(e(1:int(Tf/h)))
  allocate(i(1:int(Tf/h)))
  allocate(r(1:int(Tf/h)))
  allocate(t(1:int(Tf/h)))

  ! initial conditions
  s(1) = s0
  e(1) = e0
  i(1) = i0
  r(1) = r0
  t(1) = 0.0

  do k = 1, int(Tf/h)-1, 1

    call RK4(s(k), e(k), i(k), r(k), lambda, nu, gamma, h, s(k+1), e(k+1), i(k+1), r(k+1))
    t(k+1) = (k+1)*h

  end do

  ! save data
  open(1, file='data.dat')
  do k = 1, int(Tf/h), 1
    write (1,*) t(k), s(k), e(k), i(k), r(k)
  end do
  close(1)

  ! save model type
  open(2, file='model_type.txt')
  write(2,*) 'seir'
  close(2)


contains

  real function Sfun(s, e, i, r, lambda)
    implicit none
    real :: s, e, i, r
    real :: lambda
    Sfun = - lambda * s * i
  end function Sfun

  real function Efun(s, e, i, r, lambda, nu)
    implicit none
    real :: s, e, i, r
    real :: lambda, nu
    Efun = lambda * s * i - nu * e
  end function Efun

  real function Ifun(s, e, i, r, nu, gamma)
    implicit none
    real :: s, e, i, r
    real :: nu, gamma
    Ifun = nu * e - gamma * i
  end function Ifun

  real function Rfun(s, e, i, r, gamma)
    implicit none
    real :: s, e, i, r
    real :: gamma
    Rfun = gamma * i
  end function Rfun

  subroutine RK4(s, e, i, r, lambda, nu, gamma, h, s_n, e_n, i_n, r_n)
    implicit none
    real :: s, e, i, r, lambda, nu, gamma, h, s_n, e_n, i_n, r_n
    real :: k1_s, k2_s, k3_s, k4_s, k1_e, k2_e, k3_e, k4_e, k1_i, k2_i, k3_i, k4_i, k1_r, k2_r, k3_r, k4_r

    k1_s = Sfun(s, e, i, r, lambda)
    k1_e = Efun(s, e, i, r, lambda, nu)
    k1_i = Ifun(s, e, i, r, nu, gamma)
    k1_r = Rfun(s, e, i, r, gamma)

    k2_s = Sfun(s+k1_s*h/2.0, e+k1_e*h/2.0, i+k1_i*h/2.0, r+k1_r*h/2.0, lambda)
    k2_e = Efun(s+k1_s*h/2.0, e+k1_e*h/2.0, i+k1_i*h/2.0, r+k1_r*h/2.0, lambda, nu)
    k2_i = Ifun(s+k1_s*h/2.0, e+k1_e*h/2.0, i+k1_i*h/2.0, r+k1_r*h/2.0, nu, gamma)
    k2_r = Rfun(s+k1_s*h/2.0, e+k1_e*h/2.0, i+k1_i*h/2.0, r+k1_r*h/2.0, gamma)

    k3_s = Sfun(s+k2_s*h/2.0, e+k2_e*h/2.0, i+k2_i*h/2.0, r+k2_r*h/2.0, lambda)
    k3_e = Efun(s+k2_s*h/2.0, e+k2_e*h/2.0, i+k2_i*h/2.0, r+k2_r*h/2.0, lambda, nu)
    k3_i = Ifun(s+k2_s*h/2.0, e+k2_e*h/2.0, i+k2_i*h/2.0, r+k2_r*h/2.0, nu, gamma)
    k3_r = Rfun(s+k2_s*h/2.0, e+k2_e*h/2.0, i+k2_i*h/2.0, r+k2_r*h/2.0, gamma)

    k4_s = Sfun(s+k3_s*h, e+k3_e*h, i+k3_i*h, r+k3_r*h, lambda)
    k4_e = Efun(s+k3_s*h, e+k3_e*h, i+k3_i*h, r+k3_r*h, lambda, nu)
    k4_i = Ifun(s+k3_s*h, e+k3_e*h, i+k3_i*h, r+k3_r*h, nu, gamma)
    k4_r = Rfun(s+k3_s*h, e+k3_e*h, i+k3_i*h, r+k3_r*h, gamma)

    s_n = s + h * (k1_s + 2*k2_s + 2*k3_s + k4_s)/6.0
    e_n = e + h * (k1_e + 2*k2_e + 2*k3_e + k4_e)/6.0
    i_n = i + h * (k1_i + 2*k2_i + 2*k3_i + k4_i)/6.0
    r_n = r + h * (k1_r + 2*k2_r + 2*k3_r + k4_r)/6.0

  end subroutine RK4

end program seir
