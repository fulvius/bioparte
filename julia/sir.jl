using Plots
using DifferentialEquations
using LaTeXStrings

################################################################################

function sir!(du, u, p, t)

    # u[1]: S
    # u[2]: I
    # u[3]: R
    λ, γ = p

    du[1] = - λ * u[2] * u[1]
    du[2] = λ * u[2] * u[1] - γ * u[2]
    du[3] = γ * u[2]
end

################################################################################

function plotData(sol)

    plt = plot(sol);
    plt.series_list[1][:label] = L"S(t)";
    plt.series_list[2][:label] = L"I(t)";
    plt.series_list[3][:label] = L"R(t)";
    plt.series_list[1][:linecolor] = :blue;
    plt.series_list[2][:linecolor] = :orange;
    plt.series_list[3][:linecolor] = :red;
    xlabel!(L"t");
    display(plt)

end

################################################################################

# rates
λ = 0.00035;
γ = 0.1;
p = [λ, γ];

# time span
t = (0.0,30.0);

# initial conditions
u0 = [10000.0, 1.0, 0.0];

prob = ODEProblem(sir!, u0, t, p);
sol = solve(prob);

plotData(sol)
