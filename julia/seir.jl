using Plots
using DifferentialEquations
using LaTeXStrings

################################################################################

function seir!(du, u, p, t)

    # u[1]: S
    # u[2]: E
    # u[3]: I
    # u[4]: R

    λ, ν, γ = p

    du[1] = - λ * u[3] * u[1]
    du[2] = λ * u[3] * u[1] - ν * u[2]
    du[3] = ν * u[2] - γ * u[3]
    du[4] = γ * u[3]
end

################################################################################

function plotData(sol)

    plt = plot(sol);
    plt.series_list[1][:label] = L"S(t)";
    plt.series_list[2][:label] = L"E(t)";
    plt.series_list[3][:label] = L"I(t)";
    plt.series_list[4][:label] = L"R(t)";
    plt.series_list[1][:linecolor] = :blue;
    plt.series_list[2][:linecolor] = :green;
    plt.series_list[3][:linecolor] = :orange;
    plt.series_list[4][:linecolor] = :red;
    xlabel!(L"t");
    display(plt)

end

################################################################################

# rates
λ = 0.00035;
ν = 0.005;
γ = 0.1;
p = [λ, ν, γ];

# time span
t = (0.0,500.0);

# initial conditions
u0 = [10000.0, 1.0, 0.0, 0.0];

prob = ODEProblem(seir!, u0, t, p);
sol = solve(prob);

plotData(sol)
