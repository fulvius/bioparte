using Plots
using DifferentialEquations
using LaTeXStrings

################################################################################

function sirv!(du, u, p, t)

    # u[1]: S
    # u[2]: I
    # u[3]: R
    # u[4]: V

    λ, γ, β = p

    du[1] = - λ * u[2] * u[1] - β * u[4]
    du[2] = λ * u[2] * u[1] - γ * u[2]
    du[3] = γ * u[2]
    du[4] = β * u[4]
end

################################################################################

function plotData(sol)

    plt = plot(sol);
    plt.series_list[1][:label] = L"S(t)";
    plt.series_list[2][:label] = L"I(t)";
    plt.series_list[3][:label] = L"R(t)";
    plt.series_list[4][:label] = L"V(t)";
    plt.series_list[1][:linecolor] = :blue;
    plt.series_list[2][:linecolor] = :orange;
    plt.series_list[3][:linecolor] = :red;
    plt.series_list[4][:linecolor] = :green;
    xlabel!(L"t");
    display(plt)

end

################################################################################

# rates
λ = 0.00035;
γ = 0.1;
β = 0.4;
p = [λ, ν, β];

# time span
t = (0.0,25.0);

# initial conditions
u0 = [10000.0, 1.0, 0.0, 100.0];

# stop condition
condition(u, t, integrator) = u[1]; # E(t) = 0
affect!(integrator) = terminate!(integrator);
cb = ContinuousCallback(condition, nothing, affect!);

prob = ODEProblem(sirv!, u0, t, p, callback=cb);
sol = solve(prob);

plotData(sol)
