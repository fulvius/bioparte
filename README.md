# BIOPARTE

BIOPARTE (BIOlogical comPARTment modEls) is a package that provides biological compartment systems used for differet studyings, such as epidemics models, deseases, drugs, and human biological models.

## List of implemented models

- SIR
- SIR with vital dynamics
- SEIR with vital dynamics
- SIRV with vital dynamics

## Environments

- Mathematica
- Python
- Matlab
- Fortran
- Julia
- C++
