#include <iostream>
#include <vector>
#include <fstream>

#include <boost/numeric/odeint.hpp>

using namespace std;
using namespace boost::numeric::odeint;

typedef std::vector <double> state_type;

const double lambda_ = 0.00035;
const double gamma_ = 0.1;

////////////////////////////////////////////////////////////////////////////////

// model
void sir(const state_type &x, state_type &dxdt, const double /* t*/)
{
  dxdt[0] = - lambda_ * x[0] * x[1];
  dxdt[1] = lambda_ * x[0] * x[1] - gamma_ * x[1];
  dxdt[2] = gamma_ * x[1];
}

////////////////////////////////////////////////////////////////////////////////

// data observer
struct pushBackData
{
  std::vector <state_type> &m_states;
  std::vector <double> &m_times;

  pushBackData(std::vector <state_type> &states, std::vector <double> &times)
  : m_states(states), m_times(times) {}

  void operator () (const state_type &x, double t)
  {
    m_states.push_back(x);
    m_times.push_back(t);
  }
};

////////////////////////////////////////////////////////////////////////////////
int main()
{
  double dt = 0.01;
  double tf = 25.0;

  state_type x(3);
  // initial condition
  x[0] = 10000.0;
  x[1] = 1.0;
  x[2] = 0.0;

  // to store data
  vector <state_type> X;
  vector <double> T;

  // define stepper
  runge_kutta4 <state_type> stepper;

  // solve ODE wirh constant step size
  size_t sol = integrate_const(stepper, sir, x, 0.0, tf, dt, pushBackData(X,T));

  // save model
  ofstream outdata;
  outdata.open("model_type.txt");
  outdata << "sir";
  outdata.close();

  // save data
  outdata.open("data.dat");
  for (size_t i=0; i<=sol; i++)
    outdata << T[i] << " " << X[i][0] << " " << X[i][1] << " " << X[i][2] << endl;
  outdata.close();

  return 0;
}
