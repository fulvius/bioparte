function plotData()

    % plot data generated with fortran code

    data = readtable('data.dat');
    filename = fopen('model_type.txt','r');
    model = fscanf(filename,'%s');

    lw = 1.5;
    font_size = 12;

    orange = [255, 153, 0]/255;
    green = [0, 153, 51]/255;
    
    figure(1)
    clf
    hold on

    switch (model)

        case 'sir'

            t = data.Var1;
            s = data.Var2;
            i = data.Var3;
            r = data.Var4;

            plot(t,s,'b','LineWidth',lw)
            plot(t,i,'Color',orange,'LineWidth',lw)
            plot(t,r,'r','LineWidth',lw)
            legend('$S(t)$','$I(t)$','$R(t)$','Interpreter','Latex','FontSize',font_size-2)

        case 'seir'
            
            t = data.Var1;
            s = data.Var2;
            e = data.Var3;
            i = data.Var4;
            r = data.Var5;

            
            plot(t,s,'b','LineWidth',lw)
            plot(t,e,'Color',green,'LineWidth',lw)
            plot(t,i,'Color',orange,'LineWidth',lw)
            plot(t,r,'r','LineWidth',lw)
            legend('$S(t)$','$E(t)$','$I(t)$','$R(t)$','Interpreter','Latex','FontSize',font_size-2)

        case 'sirv'
            
            t = data.Var1;
            s = data.Var2;
            i = data.Var3;
            r = data.Var4;
            v = data.Var5;

            plot(t,s,'b','LineWidth',lw)
            plot(t,i,'Color',orange,'LineWidth',lw)
            plot(t,r,'r','LineWidth',lw)
            plot(t,v,'Color',green,'LineWidth',lw)
            legend('$S(t)$','$I(t)$','$R(t)$','$V(t)$','Interpreter','Latex','FontSize',font_size-2)

    end

    grid on
    xlabel('$t$ (s)','Interpreter','Latex','FontSize',font_size)

end