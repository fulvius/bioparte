function sir()    

    % sir model

    % x(1): s
    % x(2): i
    % x(3): r

    tmp = get(groot,'Screensize');

    font_size = 14;
    lw = 1.5;
    orange = [255,102,0]/255;

    width = 700;
    height = width - 160;
    
    dt = 0.01;
    Tf = 50;
    t = 0:dt:Tf;

    % rates
    lambda = 0.00035;
    gamma = 0.1;
    
    % initial conditions
    x0 = [10000, 1, 0];

    % system of equations
    f = @(t,x) [-lambda*x(1)*x(2);
                lambda*x(1)*x(2) - gamma*x(2);
                gamma*x(2)];

    [t, sol] = ode45(f, t, x0);

    figure(1)
    clf
    set(gcf,'Position',[tmp(3)/3, tmp(4)/3, width, height])
    hold on
    plot(t, sol(:,1),'b','LineWidth',lw)
    plot(t, sol(:,2),'Color',orange,'LineWidth',lw)
    plot(t, sol(:,3),'r','LineWidth',lw)
    xlabel('$t$ (s)','Interpreter','Latex','FontSize',font_size)
    legend('$S(t)$','$I(t)$','$R(t)$','Interpreter','Latex','FontSize',font_size-2)
    grid on


end