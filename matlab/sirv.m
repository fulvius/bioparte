function sirv()

    % sir model with vital dynamics
    
    % x(1): a
    % x(2): s
    % x(3): i
    % x(4): r
    % x(5): v

    tmp = get(groot,'Screensize');

    font_size = 14;
    lw = 1.5;
    orange = [255,102,0]/255;
    green = [0,153,51]/255;

    width = 700;
    height = width - 160;
    
    dt = 0.01;
    Tf = 50;
    t = 0:dt:Tf;

    % rates
    lambda = 0.00035;
    gamma = 0.1;
    beta = 0.1;
    b = 10^-3;
    mu_s = 10^-4;
    mu_i = 2*10^-4;
    mu_r = 4*10^-4;
    mu_v = 10^-4;
    
    % initial conditions
    x0 = [1, 10000, 1, 0, 1];
    
    % stop condition
    opt = odeset('Event',@stopSim);

    % system of equations
    f = @(t,x) [b*x(1);
                x(1)-lambda*x(2)*x(3) - mu_s*x(2) - beta*x(5);
                lambda*x(2)*x(3) - gamma*x(3) - mu_i*x(3);
                gamma*x(3) - mu_r*x(4);
                beta*x(5) - mu_v*x(5)];

    [t, sol] = ode45(f, t, x0, opt);

    figure(1)
    clf
    set(gcf,'Position',[tmp(3)/3, tmp(4)/3, width, height])
    hold on
    plot(t, sol(:,2),'b','LineWidth',lw)
    plot(t, sol(:,3),'Color',orange,'LineWidth',lw)
    plot(t, sol(:,4),'r','LineWidth',lw)
    plot(t, sol(:,5),'Color',green,'LineWidth',lw)
    xlabel('$t$ (s)','Interpreter','Latex','FontSize',font_size)
    legend('$S(t)$','$I(t)$','$R(t)$','$V(t)$','Interpreter','Latex','FontSize',font_size-2)
    grid on
    
end


function [value, isterminal, direction] = stopSim(t, x)
    
    % stop simulation when s is <= 0
    value = (x(2) <= 0);
    isterminal = 1;
    direction = 0;

end
    
    
    